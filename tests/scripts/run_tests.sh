#!/bin/bash -x

RETVAL=0

dnf -y install expect beakerlib make restraint-rhts

. /usr/share/beakerlib/beakerlib.sh
if rlIsRHEL '>=9'; then
    dnf --enablerepo rhel-buildroot install python3-pytest -y
fi

# RHEL10 cloud-init disables password authentication
# tlog tests need this to run currently
cat > /etc/ssh/sshd_config.d/00-tlog-override.conf <<EOF
PasswordAuthentication yes
EOF
chmod 600 /etc/ssh/sshd_config.d/00-tlog-override.conf
systemctl restart sshd

git clone https://github.com/Scribery/tlog.git
pushd tlog/src/tlitest
./tlitest-setup
./tlitest-run --junit-xml=/tmp/artifacts/junit.xml $*
#./teltest-teardown
RETVAL=$(( RETVAL + $? ))
popd

exit $RETVAL
